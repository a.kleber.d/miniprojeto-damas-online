#coding: utf-8

import pygame

# COLORS = {'ENABLED': (0,0,0), 'DISABLED': (255,255,255), 1: (255, 0, 0), 2: (0, 255, 0)}
COLORS = {'ENABLED': (2, 22, 38), 'DISABLED': (144, 202, 249), 1: (255, 87, 34), 2: (141, 110, 99), 'CROWN': (255,255,0), 'MOVES': (0, 200, 0),'EATS': (100, 0, 0)}

APPLICATION = {'FPS': 30, 'NAME': 'Damas'}
# CONFIG = {'CELL_SIZE': 50, 'CELL_PADDING': 6, 'LINE_SIZE': 11, 'PIECES': 19, 'CROWN_WIDTH': 18, 'CROWN_HEIGHT': 4}
CONFIG = {'CELL_SIZE': 75, 'CELL_PADDING': 6, 'LINE_SIZE': 8, 'PIECES': 12, 'CROWN_WIDTH': 18, 'CROWN_HEIGHT': 4}
PLAYER = {'NAME': 1}
# CONFIG = {'CELL_SIZE': 75 , 'LINE_SIZE': 8, 'PIECES': 12}
APPLICATION['WIDTH'] = APPLICATION['HEIGHT'] = CONFIG['CELL_SIZE'] * CONFIG['LINE_SIZE']

STATE = {'MOUSE_PRES': False, 'MY_TURN': True, 'SELECTED': False, 'SELECTED_EATS': {}, 'SELECTED_MOVES': []}

def template():
    tab = []
    for i in range(CONFIG['LINE_SIZE']):
        tab.append([])
        for j in range(CONFIG['LINE_SIZE']):
            tab[i].append(False)
            if i % 2 == j % 2:
                tab[i][j] = True

    return tab

def symetricSwapUtil(elem):
    elem['owner'] = 3 - elem['owner']

def symetricSwap(arr, i, j, n):
    if type(arr[i][j]) is dict:
        symetricSwapUtil(arr[i][j])
    if type(arr[n - i - 1][n - j - 1]) is dict:
        symetricSwapUtil(arr[n - i - 1][n - j - 1])
    temp = arr[i][j]
    arr[i][j] = arr[n - i - 1][n - j - 1]
    arr[n - i - 1][n - j - 1] = temp
    # print i, ' ', j, ' - ', n - i - 1, ' ', n - j - 1

def rotate(template):
    for i in range(CONFIG['LINE_SIZE'] // 2):
        for j in range(CONFIG['LINE_SIZE']):
            symetricSwap(template, i, j, CONFIG['LINE_SIZE'])

def halffill(template):
    n = CONFIG['PIECES']
    for i in range(CONFIG['LINE_SIZE'] - 1, -1, -1):
        for j in range(CONFIG['LINE_SIZE'] - 1, -1, -1):
            if template[i][j]:
                template[i][j] = {'king': False, 'owner': 1}
                # print i, j
                n -= 1

            if n == 0: break
        if n == 0: break

def fullfill(template):
    halffill(template)
    # print template[0]
    # for i in template:
    #     print i
    rotate(template)
    halffill(template)

def onClick():
    # print 'must eat: ', mustEat()
    if STATE['MOUSE_PRES'] or not STATE['MY_TURN']:
        return

    pos = pygame.mouse.get_pos()
    x  = pos[1] // CONFIG['CELL_SIZE']
    y  = pos[0] // CONFIG['CELL_SIZE']

    if (x, y) in STATE['SELECTED_EATS']:
        # print 'hora da comilança'
        eatPiece(x, y)
        return

    elif not mustEat() and (x, y) in STATE['SELECTED_MOVES']:
        # print 'vamos andar um poquito pacito pacito'
        movePiece(x, y)
        return
    elif STATE['SELECTED'] == (x, y):
        # print 'not today'
        return

    if type(board[x][y]) is not dict:
        # print 'I die here'
        return

    STATE['SELECTED'] = (x, y)

    STATE['SELECTED_EATS'] = eatsPositions(x, y)

    if len(STATE['SELECTED_EATS']) == 0:
        STATE['SELECTED_MOVES'] = movesPositions(x, y)

    STATE['MOUSE_PRES'] = True

def actionDone():
    # STATE['MY_TURN'] = False
    STATE['SELECTED'] = False
    STATE['SELECTED_EATS'] = {}
    STATE['SELECTED_MOVES'] = []

def eatPiece(x, y):
    i, j = STATE['SELECTED']

    # Move piece
    board[x][y] = board[i][j]
    board[i][j] = True

    # Kill enemy
    i, j = STATE['SELECTED_EATS'][(i, j)]
    board[i][j] = True

    actionDone()

def movePiece(x, y):
    # print 'I wanna mooooooooove'
    i, j = STATE['SELECTED']

    board[x][y] = board[i][j]
    board[i][j] = True

    actionDone()

def eatsPositions(x, y, king=False):
    eats = {}
    if not king:
        for i in range(-1, 2, 2):
            for j in range(-1, 2, 2):
                if isPositionValid(x + i, y + j) and type(board[x + i][y + j]) is dict \
                    and board[x + i][y + j]['owner'] != PLAYER['NAME'] \
                    and isPositionValid(x + i * 2, y + j * 2) and not board[x + i * 2][y + j * 2]:
                    eats[(x + i * 2, y + j * 2)] = (x + i, y + j)
        return eats
    # TODO: kigs movemments

def mustEat():
    for i in range(CONFIG['LINE_SIZE']):
        for j in range(CONFIG['LINE_SIZE']):
            if type(board[i][j]) is dict and len(eatsPositions(i, j)) > 0:
                # print 'mandatorys: ', i, j, eatsPositions(i, j), board[i][j]
                return True
    return False

def movesPositions(x, y, king=False):
    moves = []
    if not king:
        for j in range(-1, 2, 2):
            if isPositionValid(x - 1, y + j) and type(board[x - 1][y + j]) is not dict:
                moves.append((x - 1, y + j))
        return moves
    # TODO: kigs movemments

def isPositionValid(x, y):
    return x >= 0 and y >= 0 and x < CONFIG['LINE_SIZE'] and y < CONFIG['LINE_SIZE']

board = template()
fullfill(board)

pygame.init()

gameDisplay = pygame.display.set_mode((APPLICATION['WIDTH'], APPLICATION['HEIGHT']))
pygame.display.set_caption(APPLICATION['NAME'])

clock = pygame.time.Clock()

exitGame = False

boardTemplate = template()

while not exitGame:
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            onClick()

        if event.type == pygame.MOUSEBUTTONUP:
            STATE['MOUSE_PRES'] = False

        if event.type == pygame.QUIT:
            pygame.quit()
            quit(0)

        for i in range(CONFIG['LINE_SIZE']):
            for j in range(CONFIG['LINE_SIZE']):
                col = COLORS['DISABLED']

                if boardTemplate[j][i]:
                    col = COLORS['ENABLED']

                if (j, i) in STATE['SELECTED_MOVES']:
                    col = COLORS['MOVES']

                if (j, i) in STATE['SELECTED_EATS']:
                    col = COLORS['EATS']

                x_pos = i * CONFIG['CELL_SIZE']
                y_pos = j * CONFIG['CELL_SIZE']
                wid = heig = CONFIG['CELL_SIZE']
                pygame.draw.rect(gameDisplay, col, [x_pos, y_pos, wid, heig])

                # Start drawing table pice if there is some of it

                if type(board[j][i]) is dict:
                    # print i, j
                    col = COLORS[board[j][i]['owner']]
                    x_pos = i * CONFIG['CELL_SIZE'] + CONFIG['CELL_SIZE'] // 2
                    y_pos = j * CONFIG['CELL_SIZE'] + CONFIG['CELL_SIZE'] // 2
                    radius = CONFIG['CELL_SIZE'] // 2 - CONFIG['CELL_PADDING']
                    pygame.draw.circle(gameDisplay, col, [x_pos, y_pos], radius)

                    if board[j][i]['king']:
                        col = COLORS['CROWN']

                        x_pos = i * CONFIG['CELL_SIZE'] + CONFIG['CELL_SIZE'] // 2
                        y_pos = j * CONFIG['CELL_SIZE'] + CONFIG['CELL_SIZE'] // 2
                        radius = CONFIG['CELL_SIZE'] // 2 - 3 * CONFIG['CELL_PADDING']

                        pygame.draw.circle(gameDisplay, col, [x_pos, y_pos], radius)

    pygame.display.update()
